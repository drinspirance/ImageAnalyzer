package com.example.okorchysta.imageanalyzer.Common;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by OKorchysta on 16.11.2017.
 */

public class ImgurData {

    private List<Image> listOfDownloadedImages = new ArrayList<>();
    private int lastDownloadedPage;
    private int lastShownItem;

    public static final ImgurData instance = new ImgurData();

    public static ImgurData getInstance() {
        return instance;
    }

    private ImgurData(){

    }


    public List<Image> getListOfDownloadedImages() {
        return listOfDownloadedImages;
    }

    public void addToListOfDownloadedImages(List<Image> list) {
        listOfDownloadedImages.addAll(list);
    }

    public void addToListOfDownloadedImages(Image image){
        listOfDownloadedImages.add(image);
    }

    public int getLastDownloadedPage() {
        return lastDownloadedPage;
    }

    public void setLastDownloadedPage(int lastDownloadedPage) {
        this.lastDownloadedPage = lastDownloadedPage;
    }

    public int getLastShownItem() {
        return lastShownItem;
    }

    public void setLastShownItem(int lastShownItem) {
        this.lastShownItem = lastShownItem;
    }
}
