package com.example.okorchysta.imageanalyzer.Client;

import android.os.Bundle;

/**
 * Created by OKorchysta on 13.11.2017.
 */

public interface IMainView {
    void onError();
    void onSuccess(Bundle resultData);
}
