package com.example.okorchysta.imageanalyzer.Service.model;

import android.util.Log;

import com.example.okorchysta.imageanalyzer.Common.Constants;
import com.example.okorchysta.imageanalyzer.Common.Image;
import com.example.okorchysta.imageanalyzer.Common.ImgurData;
import com.example.okorchysta.imageanalyzer.ResultObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by OKorchysta on 14.11.2017.
 */

public class ServiceModel implements IServiceModel {

    private static final String TAG = "ServiceModel";
    HttpURLConnection urlConnection;


    @Override
    public ResultObject getDataFromImgurPage(URL url) {
        Log.d(TAG, "---->getDataFromImgurPage: ");

        int responseCode = 500;
        StringBuilder response = new StringBuilder();
        JSONArray jsonArray;

        try {
            //open connection
            urlConnection =  (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.setRequestProperty(Constants.AUTHORIZATION_KEY, Constants.CLIENT_ID);

            responseCode = urlConnection.getResponseCode();
            Log.d(TAG, "Sending GET request to URL : " +    url + ". ResponseCode is "+responseCode);

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(urlConnection.getInputStream()));
            String inputLine;
            response = new StringBuilder();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            System.out.println("Response : -- " + response.toString());

        }catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }

        try {
            JSONObject jsonObject = new JSONObject(response.toString());
            jsonArray = jsonObject.getJSONArray("data");
            Log.d(TAG, "responseCode is: " + responseCode);
            if (responseCode==200){
                Log.d(TAG, "getDataFromImgurPage<----");
                return new ResultObject(ResultObject.CODE.OK, jsonArray);
            } else {
                Log.d(TAG, "getDataFromImgurPage<----");
                return new ResultObject(ResultObject.CODE.IMGUR_ERROR, null);
            }
        } catch (JSONException ex){
            Log.d(TAG, "JSONException: " + ex);
            Log.d(TAG, "getDataFromImgurPage<----");
            return new ResultObject(ResultObject.CODE.IMGUR_ERROR, null);
        }

    }

    @Override
    public List<Image> getImageList(JSONArray jsonArray) {
        Log.d(TAG, "---->getImageList");
        List<Image> imageList = new ArrayList<>();
        try {
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);

                String id;
                String link;
                if (jsonObject.getBoolean("is_album")){
                    JSONObject jsonImage = jsonObject.getJSONArray("images").getJSONObject(0);
                    id = jsonImage.getString("id");
                    link = jsonImage.getString("link");

                    if ( jsonImage.getString("type").equals("image/gif"))
                        continue;
                } else {

                    if ( jsonObject.getString("type").equals("image/gif"))
                        continue;

                    id = jsonObject.getString("id");
                    link = jsonObject.getString("link");
                }

                String title = jsonObject.getString("title");
                long datetime =  jsonObject.getLong("datetime");
                long views = jsonObject.getLong("views");
                long points = jsonObject.getLong("points");

                Image image = new Image(id, title, datetime, views, points, link, null);
                imageList.add(image);
                Log.d(TAG, "image added to list: " + image.toString());
            }

        } catch (JSONException ex){
            Log.d(TAG, "Exception during getting Images from JSON: " + ex);
            return null;
        }

        ImgurData.getInstance().addToListOfDownloadedImages(imageList);

        return imageList;
    }
}
