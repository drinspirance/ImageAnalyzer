package com.example.okorchysta.imageanalyzer.Common;

/**
 * Created by OKorchysta on 14.11.2017.
 */

public class Constants {

    private Constants(){}

    public static final String AUTHORIZATION_KEY = "Authorization";
    public static final String CLIENT_ID = "Client-ID 47b1a69317f93cb";

    public static final int SERVICE_ERROR = 0;
    public static final int SERVICE_RECEIVE_LIST = 1;

}
