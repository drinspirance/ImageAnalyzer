package com.example.okorchysta.imageanalyzer.Service.presenter;

import com.example.okorchysta.imageanalyzer.Common.Image;
import com.example.okorchysta.imageanalyzer.ResultObject;

import org.json.JSONArray;

import java.net.URL;
import java.util.List;

/**
 * Created by OKorchysta on 14.11.2017.
 */

public interface IServicePresenter {

    ResultObject getDataFromImgurPage(URL url);
    List<Image> getImageList(JSONArray jsonArray);

}
